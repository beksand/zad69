import java.util.ArrayList;
import java.util.List;

public class Main {

//    Utwórz klasę Student, która posiada pola: wiek, imię, nazwisko, nr. indexu
//    a. Utwórz 20 obiektów klasy student z losowymi danymi (imiona studentów, które możesz użyć: Ala, Mateusz, Jan,
// Przemysław, Mariola; nazwiska - Kowalski/a, Nowogrodzki/a, Nowak, Makacki/a)
//    b. Posortuj w/w klasę według nr indexu wykorzystując algorytm merge sort
//    c. Wyświetl tablicę przed i po sortowaniu

    public static void main(String[] args) {
	// write your code here
        List<Student> students = new ArrayList<>();
        students.add(new Student(1,17, "Ala", "Kowalska"));
        students.add(new Student(23,17, "Mateusz", "Nowogrodzki"));
        students.add(new Student(2,17, "Jan", "Kowalski"));
        students.add(new Student(8,17, "Przemysław", "Makacki"));
        students.add(new Student(4,17, "Mariola", "Nowak"));
        students.add(new Student(16,17, "Ala", "Kowalska"));
        students.add(new Student(9,17, "Ala", "Kowalska"));
        students.add(new Student(10,17, "Ala", "Kowalska"));
        students.add(new Student(12,17, "Ala", "Kowalska"));
        students.add(new Student(19,17, "Ala", "Kowalska"));
        students.add(new Student(3,17, "Ala", "Kowalska"));
        students.add(new Student(5,17, "Ala", "Kowalska"));
        students.add(new Student(6,17, "Ala", "Kowalska"));
        students.add(new Student(7,17, "Ala", "Kowalska"));
        students.add(new Student(17,17, "Ala", "Kowalska"));
        students.add(new Student(0,17, "Ala", "Kowalska"));
        System.out.println(students);


    }
}
